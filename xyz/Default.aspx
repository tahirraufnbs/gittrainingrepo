﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Intelligize.Webparts.App.Default" %>
<%@ Import Namespace="Intelligize.Webparts.App.Code" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>WebPart</title>

    <link href="Style/token-input-facebookMain.css" rel="stylesheet" type="text/css"/>
    <link href="Style/token-input-mac.css" rel="stylesheet" type="text/css"/>
    <link href="Style/token-input.css" rel="stylesheet" type="text/css"/>
    <link href="Style/webpart.css" rel="stylesheet"/>
    <script src="Scripts/jquery.min.js" type="text/javascript"></script>
    <%--<script type="text/javascript"  src="Scripts/jquery-1.4.1.min.js"></script>
    <script type="text/javascript"  src="Scripts/jquery-1.4.1.js"></script>--%>
    <script type="text/javascript" src="Scripts/jquery.tokeninput.js"></script>
    <script src="Scripts/jquery.autocomplete.js" type="text/javascript"></script>
    <script type="text/javascript" src="Scripts/query.js"></script>
    <script type="text/javascript">


        $(document).ready(function() {

            var companyCik = [];
            var filter = JSON.stringify({ "filter": "Not a valid filter" });

            $.ajax({
                type: "POST",
                data: filter,
                url: "Default.aspx/GetCompaniesAutoComplete",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: SetAutoComplete
            });

            function SetAutoComplete(result) {
                var jsonObject = JSON.parse(result.d); //parsing json string returned from Ajax
                $("#txtCompany").tokenInput(jsonObject, {
                    theme: "facebookMain",
                    hintText: "Enter Company Name, Ticker or CIK",
                    showHintAsWatermark: true,
                    onAdd: function(item) {
                        var exist = false;

                        // If that value is already present in the main list then no need of adding
                        var i = 0
                        for (; i < companyCik.length; i++) {
                            if (companyCik[i].id == item.id) {
                                exist = true;
                                break;
                            }
                        }

                        // It will add the specific value if not exist
                        if (!exist) {
                            item.type = "add";
                            companyCik.push(item);
                        }

                        // $.cookie("CompanyAutoCompleteInfo", companyCik);
                    },
                    onDelete: function(item) {
                        var exist = false;

                        // Check if that value is present in the main list 
                        var i = 0;
                        for (; i < companyCik.length; i++) {
                            if (companyCik[i].id == item.id) {
                                exist = true;
                                break;
                            }
                        }
                        // It will delete the specific value if exist
                        if (exist) {
                            companyCik.splice(i, 1);
                        }

                    }
                });
            }
        });

        function getValue() {
            var ddl = document.getElementById("userOptions");
            var selectedvalue = ddl.value;
            return selectedvalue;
        }

        function DoPostBack(control, value) {
            __doPostBack(control, value);
        }

    </script>

    <style type="text/css">
        #dataTable {
            border: 1px solid #CECECE;
            border-collapse: collapse;
        }

        #dataTable tr td {
            border-right: 1px solid #CECECE;
            border-bottom: 1px solid #CECECE;
        }

        #pagination_table tbody tr td { color: #66605C; }

        a.linkbutton, a.linkbutton:visited {
            color: #66605c;
            cursor: pointer;
            font-size: 11px;
            font-weight: normal;
            text-decoration: underline;
        }

        span.textbutton {
            color: #66605C;
            font-size: 11px;
            font-weight: normal;
        }

        #textbox_query { width: 631px; }

        .style1 { width: 648px; }
    </style>

</head>
<body style="height: 132px">

<form id="myForm" runat="server" style="padding-left: 20px; padding-right: 20px;">
<asp:ScriptManager ID="ScriptManager1" AsyncPostBackTimeout="300" runat="server"></asp:ScriptManager>
<%--<asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
    <ProgressTemplate>
      Update in progress...
    </ProgressTemplate>
    </asp:UpdateProgress>--%>
<table cellspacing="1" cellpadding="3" border="0" align="center"
       style="width: 52%; padding-left: 6px; font-family: arial; font-size: 9pt;">
    <tbody>
    <tr>

        <td align="center" class="style1">
            <table>

                <tr>

                    <td>
                        <input id="txtCompany" type="text" name="txtCompany" runat="server" style="width: 212px; height: 20px;"/>
                    </td>
                    <td>
                        <asp:Button ID="btnSearch" runat="server" Text="Button" OnClick="btnSearch_Click" UseSubmitBehavior="False" CssClass="btn-search"/>
                        <%-- <input id="btnSearch" type="submit" runat="server" value="Search"
                                        class="btn-search" />--%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="userOptions" runat="server" Height="28px" Width="252px" CssClass="form-dropdown">
                        </asp:DropDownList>
                    </td>


                </tr>
            </table>
        </td>
    </tr>
    </tbody>
</table>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<%
    if (IsPostBack && Session.Keys.Count >= 0 && Session.Keys.Count <= 3)
    {

        //string redirectURL = Page.ResolveClientUrl(url);
        string script = "alert('Page session was expired.');window.location = '" + Request.ApplicationPath + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
    string nextPageQuery = string.Empty, previousPageQuery = string.Empty, firstPageQuery = string.Empty, lastPageQuery = string.Empty;
    string pageNumber = string.Empty;
    int totalRecords = 0, nextPage = 0, previousPage = 0;
    int lastPageNumber = 0;
    List<string> sl = this.GetColumnsSequence();
    bool isFooterRequired = true;
    bool isResultFound = false;
    string pageNoServer = Session["PageNo"] != null ? Session["PageNo"].ToString() : string.Empty;
    
    List<Document> rl = GetResults(pageNoServer);
    
    string queryString = Session["SearchQuery"] == null ? "" : Session["SearchQuery"].ToString();
    if (Regex.IsMatch(queryString, @"\s*footer\s*=\s*0", RegexOptions.IgnoreCase)) // .IndexOf("footer=0",StringComparison.OrdinalIgnoreCase)>-1)
    {
        isFooterRequired = false;
    }
    if (rl != null && rl.Count > 0)
    {
        isResultFound = true;
        totalRecords = rl[0].TotalRecords;
        pageNumber = rl[0].PageNo.ToString();

        string originalQueryString = Session["SearchQuery"].ToString();
        originalQueryString = Regex.Replace(originalQueryString, @"(\&?|\?)PageNo=[^&]*", string.Empty, RegexOptions.IgnoreCase);
        originalQueryString = Regex.Replace(originalQueryString, @"(\&?|\?)filter_formtype=[^&]*", string.Empty, RegexOptions.IgnoreCase);
        originalQueryString = Regex.Replace(originalQueryString, @"(\&?|\?)filter_formgroup=[^&]*", string.Empty, RegexOptions.IgnoreCase);
        queryString = Session["SearchQuery"].ToString();
        if (Regex.IsMatch(queryString, @"\s*footer\s*=\s*0", RegexOptions.IgnoreCase)) // .IndexOf("footer=0",StringComparison.OrdinalIgnoreCase)>-1)
        {
            isFooterRequired = false;
        }
        queryString = Regex.Replace(queryString, @"(\&?|\?)PageNo=[^&]*", string.Empty, RegexOptions.IgnoreCase);
        string pageSize = Session["PageSize"].ToString();

        double lastPageValue = Convert.ToDouble(totalRecords)/Convert.ToDouble(pageSize);
        double lastPage = Math.Ceiling(lastPageValue);
        lastPageNumber = Convert.ToInt32(lastPage);

        if (!string.IsNullOrEmpty(pageNumber))
        {
            nextPageQuery = (Convert.ToInt32(pageNumber) + 1).ToString();
            previousPageQuery = (Convert.ToInt32(pageNumber) - 1).ToString();
            lastPageQuery = lastPageNumber.ToString();
            firstPageQuery = "1";
            //nextPageQuery = Convert.ToInt32(pageNumber) + 1;
            //previousPageQuery = Convert.ToInt32(pageNumber) - 1;
            //if (queryString.Contains("?"))
            //{
            //    nextPageQuery = queryString + "&PageNo=" + nextPage.ToString();
            //}
            //else
            //{
            //    nextPageQuery = queryString + "?PageNo=" + nextPage.ToString();
            //}
            //if (queryString.Contains("?"))
            //{
            //    previousPageQuery = queryString + "&PageNo=" + previousPage.ToString();
            //}
            //else
            //{
            //    previousPageQuery = queryString + "?PageNo=" + previousPage.ToString();
            //}
            //if (queryString.Contains("?"))
            //{
            //    firstPageQuery = queryString + "&PageNo=1";
            //}
            //else
            //{
            //    firstPageQuery = queryString + "?PageNo=1";
            //}
            //if (queryString.Contains("?"))
            //{
            //    lastPageQuery = queryString + "&PageNo=" + lastPageNumber.ToString();
            //}
            //else
            //{
            //    lastPageQuery = queryString + "?PageNo=" + lastPageNumber.ToString();
            //}
        }
        bool status = true;
        if ((Request["filter_formtype"] != null || Request["Filter_FormType"] != null)
            && Request["filter_formgroup"] == null && Request["Filter_FormGroup"] == null)
        {
            if (Request["filter_formtype"] != null && !Request["filter_formtype"].Contains(";"))
            {
                status = false;
            }
            else
            {
                status = true;
            }
        }
        status = true;
        if (status)
        {
            if (Session["params2"] != null && (Session["OldQueryString"].ToString().Equals(string.Empty) || Session["OldQueryString"].ToString().Equals(Session["SearchQuery"].ToString()) || Server.UrlDecode(Session["OldQueryString"].ToString()).Equals(Server.UrlDecode(Session["SearchQuery"].ToString()))))
            {
                System.Collections.Specialized.NameValueCollection collection = (System.Collections.Specialized.NameValueCollection) Session["params2"];
                userOptions.SelectedItem.Text = collection.GetValues("selectedFormValue")[0].ToString();
            }
            // List<string> forms = listObjects.Select(obj => obj.Value.ToString()).ToList<string>().Distinct().ToList<string>();
%>

    <%
        }
    %>


    <table cellspacing="1" cellpadding="3" border="0" align="center" style="font-family: arial, sans-serif; font-size: 62.5%; min-width: 348px;">
        <tbody>
        <tr>
            <td align="right" style="width: 867px;">
                <%
                    if (!string.IsNullOrEmpty(pageNumber) && Convert.ToInt32(pageNumber) > 1)
                    { %>
                    <span class="ccbnNav">&lt;&lt;&nbsp;<a onclick="DoPostBack('aFirst', '<%= firstPageQuery %>')" id ="aFirst" <%--href="<%=firstPageQuery %>"--%> class="linkbutton">First</a></span>
                <%
                    }
                    else
                    {
                %>
                    <span class="textbutton">&lt;&lt;&nbsp;First</span>
                <%
                    }
                    if (!string.IsNullOrEmpty(pageNumber) && Convert.ToInt32(pageNumber) > 1)
                    {
                %>
                    <span class="ccbnNav">&nbsp;|&nbsp;<a <%--href="<%=previousPageQuery %>"--%> onclick="DoPostBack('aPrevious', '<%= previousPageQuery %>')" id ="aPrevious" class="linkbutton">Previous</a></span>
                <%
                    }
                    else
                    { %>
                    <span class="textbutton">&nbsp;|&nbsp;Previous</span>
                <% }
                    if (!string.IsNullOrEmpty(pageNumber) && totalRecords > Convert.ToInt32(pageSize) && lastPageNumber != Convert.ToInt32(pageNumber))
                    { %>

                    <span class="ccbnNav">&nbsp;|&nbsp;
                                <%--href="<%=nextPageQuery %>" <asp:LinkButton runat="server" CssClass="linkbutton" CommandName="next" ID="linkNext" OnClick="Unnamed1_Click">LinkButton</asp:LinkButton>--%>
                                <a  ID="aNext" onclick="DoPostBack('aNext', '<%= nextPageQuery %>')" class="linkbutton">Next</a>

                            </span>
                <%
                    }
                    else
                    { %>
                    <span class="textbutton">&nbsp;|&nbsp;Next</span>
                <%
                    }
                    if (lastPageNumber > 0 && lastPageNumber > Convert.ToInt32(pageNumber))
                    {
                %>
                    <span class="ccbnNav">&nbsp;|&nbsp;<a <%--href="<%=lastPageQuery %>" --%> onclick="DoPostBack('aLast', '<%= lastPageQuery %>')" id="aLast" class ="linkbutton">Last</a>&nbsp;&gt;&gt;</span>

                <%
                    }
                    else
                    {
                %>
                    <span class="textbutton">&nbsp;|&nbsp;Last&nbsp;&gt;&gt;</span>
                <%
                    }
                %>
            </td>
        </tr>
        </tbody>
    </table>


    <div id="table_div">

        <%
            if (rl != null && rl.Count() > 0)
            {
        %>
            <table id="dataTable" align="center" cellspacing="1" cellpadding="3" style="min-width: 348px; font-family: arial, sans-serif; font-size: 62.5%; border-bottom: none">
                <tbody>

                <tr class="ccbnBgTblTtl" style="color: #27547D; font-weight: bold; font-size: 11px">
                    <td valign="top" nowrap="nowrap" style="width: 15%">
                        <span class="ccbnTblTtl"><%= sl[0] %></span></td>
                    <td valign="top" nowrap="nowrap" style="width: 15%">
                        <span class="ccbnTblTtl"><%= sl[1] %></span></td>
                    <td valign="top" nowrap="nowrap" style="width: 63%">
                        <span class="ccbnTblTtl"><%= sl[2] %></span></td>
                    <td valign="top" nowrap="nowrap" align="center" colspan="2" style="border-right: none">
                        <span class="ccbnTblTtl"><%= sl[3] %></span></td>
                </tr>
                <%
                    for (int i = 0; i < rl.Count(); i++)
                    {
                        string docID = rl[i].DocumentId.ToString();
                %>
                    <tr class="ccbnBgTblOdd" align="left" style="font-weight: normal; font-size: 11px; color: #66605C">
                        <td valign="middle" nowrap="nowrap" class="td-pading">
                            <span class="ccbnTblOdd">
                            <% string col0 = string.Empty;
                               switch (sl[0])
                               {
                                   case "Filing Date":
                                       if (rl[i].FilingDate != null)
                                           col0 = rl[i].FilingDate.ToString();
                                       else
                                           col0 = string.Empty;
                                       break;
                                   case "Form":
                                       if (rl[i].FormType != null)
                                           col0 = rl[i].FormType.ToString();
                                       else
                                           col0 = string.Empty;
                                       break;
                                   case "Company Name":
                                       if (rl[i].CompanyName != null)
                                           col0 = rl[i].CompanyName.ToString().ToUpper();
                                       else
                                           col0 = string.Empty;
                                       break;
                                   default:
                                       if (rl[i].FilingDate != null)
                                           col0 = rl[i].FilingDate.ToString();
                                       else
                                           col0 = string.Empty;
                                       break;
                               } %>
                            <%= col0 %>
                        </span>
                        </td>
                        <td valign="middle" nowrap="nowrap" class="td-pading">
                            <span class="ccbnTblOdd">

                            <a target="_blank" href="<%= rl[i].DocumentUrl %>" class="ccbnTblLnk" style="color: #66605C">
                                <% string col1 = string.Empty;
                                   switch (sl[1])
                                   {
                                       case "Filing Date":
                                           if (rl[i].FilingDate != null)
                                               col1 = rl[i].FilingDate.ToString();
                                           else
                                               col1 = string.Empty;
                                           break;
                                       case "Form":
                                           if (rl[i].FormType != null)
                                               col1 = rl[i].FormType.ToString();
                                           else
                                               col1 = string.Empty;
                                           break;
                                       case "Company Name":
                                           if (rl[i].CompanyName != null)
                                               col1 = rl[i].CompanyName.ToString().ToUpper();
                                           else
                                               col1 = string.Empty;
                                           break;
                                       default:
                                           if (rl[i].FilingDate != null)
                                               col1 = rl[i].FilingDate.ToString();
                                           else
                                               col1 = string.Empty;
                                           break;
                                   }
                                %>
                                <%= col1 %></a></span>
                            <% if (!string.IsNullOrEmpty(rl[i].Exhibit))
                               { %>
                                <br/>
                                <span>
                            <%= rl[i].Exhibit %>
                        </span>
                            <% } %>
                        </td>
                        <td valign="middle" class="td-pading">
                            <span class="ccbnTblOdd">
                            <% string col2 = string.Empty;
                               switch (sl[2])
                               {
                                   case "Filing Date":
                                       if (rl[i].FilingDate != null)
                                           col2 = rl[i].FilingDate.ToString();
                                       else
                                           col2 = string.Empty;
                                       break;
                                   case "Form":
                                       if (rl[i].FormType != null)
                                           col2 = rl[i].FormType.ToString();
                                       else
                                           col2 = string.Empty;
                                       break;
                                   case "Company Name":
                                       if (rl[i].CompanyName != null)
                                           col2 = rl[i].CompanyName.ToString().ToUpper();
                                       else
                                           col2 = string.Empty;
                                       break;
                                   default:
                                       if (rl[i].FilingDate != null)
                                           col2 = rl[i].FilingDate.ToString();
                                       else
                                           col2 = string.Empty;
                                       break;
                               }
                            %>
                            <%= col2 %>
                        </span>
                        </td>

                        <td valign="middle" class="td-pading" align="center">
                            <a href="<%= rl[i].RTFDocumentUrl %>" target="_blank">
                                <img border="0" alt="Download SEC Filing to Word" src="Images/word.gif"/>
                            </a>
                        </td>
                        <td valign="middle" class="td-pading" align="center" style="border-right: none;">
                            <a href="<%= rl[i].PDFDocumentUrl %>" target="_blank" style="width: 30%; height: 30%;">
                                <img border="0" alt="Download SEC Filing to Adobe PDF" src="Images/pdf.gif"/>
                            </a>
                        </td>
                    </tr>
                <%
                    }
                %>
                </tbody>
            </table>
        </div>

    <table cellspacing="1" cellpadding="3" border="0" align="center" style="font-family: arial, sans-serif; font-size: 62.5%; min-width: 348px; min-width: 348px;">
        <tbody>
        <tr>
            <td align="right">
            </td>
            <td align="right" style="width: 521px;">
                <%
                    if (!string.IsNullOrEmpty(pageNumber) && Convert.ToInt32(pageNumber) > 1)
                    { %>
                    <span class="ccbnNav">&lt;&lt;&nbsp;<a onclick="DoPostBack('aFirst', '<%= firstPageQuery %>')" id ="aFirst" <%--href="<%=firstPageQuery %>"--%> class="linkbutton">First</a></span>
                <%
                    }
                    else
                    {
                %>
                    <span class="textbutton">&lt;&lt;&nbsp;First</span>
                <%
                    }
                    if (!string.IsNullOrEmpty(pageNumber) && Convert.ToInt32(pageNumber) > 1)
                    {
                %>
                    <span class="ccbnNav">&nbsp;|&nbsp;<a <%--href="<%=previousPageQuery %>"--%> onclick="DoPostBack('aPrevious', '<%= previousPageQuery %>')" id ="aPrevious" class="linkbutton">Previous</a></span>
                <%
                    }
                    else
                    { %>
                    <span class="textbutton">&nbsp;|&nbsp;Previous</span>
                <% }
                    if (!string.IsNullOrEmpty(pageNumber) && totalRecords > Convert.ToInt32(pageSize) && lastPageNumber != Convert.ToInt32(pageNumber))
                    { %>
                    <span class="ccbnNav">&nbsp;|&nbsp;
                                <%--href="<%=nextPageQuery %>" <asp:LinkButton runat="server" CssClass="linkbutton" CommandName="next" ID="linkNext" OnClick="Unnamed1_Click">LinkButton</asp:LinkButton>--%>
                                <a  ID="aNext" onclick="DoPostBack('aNext', '<%= nextPageQuery %>')" class="linkbutton">Next</a>

                            </span>
                <%
                    }
                    else
                    { %>
                    <span class="textbutton">&nbsp;|&nbsp;Next</span>
                <%
                    }
                    if (lastPageNumber > 0 && lastPageNumber > Convert.ToInt32(pageNumber))
                    {
                %>
                    <span class="ccbnNav">&nbsp;|&nbsp;<a <%--href="<%=lastPageQuery %>" --%> onclick="DoPostBack('aLast', '<%= lastPageQuery %>')" id="aLast" class ="linkbutton">Last</a>&nbsp;&gt;&gt;</span>

                <%
                    }
                    else
                    {
                %>
                    <span class="textbutton">&nbsp;|&nbsp;Last&nbsp;&gt;&gt;</span>
                <%
                    }
                %>
            </td>
        </tr>
        <% if (isFooterRequired == true)
           { %>
            <tr>
                <td align="right" style="color: gray; width: 40%;">
                    Powered by:

                </td>
                <td>
                    <asp:Image runat="server" ImageUrl="~/Images/logo.png" Style="width: 150px;"/>
                </td>
            </tr>
        <% } %>
        </tbody>
    </table>

<%
            }
    }
    else
    {
        if (Session["NoMode"] != null && (bool) Session["NoMode"] == true && Session["params"] == null && Session["params2"] == null && Session["companyRelInfo"] == null)
        {
            // No need of showing anything
        }
        else
        {
%>

        <div id="noResults" align="center" style="font-family: Arial; font-size: 11pt; color: gray; font-weight: bold">
            <h4>No Results Found</h4>
        </div>

<%
        }
    }
    // Resetting filters params session values
    Session["companyRelInfo"] = null;
    Session["params2"] = null;
    Session["NoMode"] = null;
    Session["params"] = null;
    Session["PageNo"] = null;
    if (isFooterRequired == true && isResultFound == false)
    { %>
    <div style="width: 100%; padding-top: 50px;">
        <table align="center" style="font-family: arial, sans-serif; font-size: 62.5%;">
            <tbody>
            <tr>
                <td align="right" style="color: gray">
                    Powered by:

                </td>
                <td>
                    <img style="width: 150px;" src="Images/logo.png">
                </td>
            </tr>
            </tbody>
        </table>
    </div>
<% }
%>
</ContentTemplate>

<Triggers>
    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click"/>
</Triggers>

</asp:UpdatePanel>
</form>
</body>
</html>