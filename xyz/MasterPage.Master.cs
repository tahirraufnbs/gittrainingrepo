﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;

namespace DebevoiseWebView
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        SessionParameter sm = new SessionParameter();        
      
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Collections.Specialized.NameValueCollection collection;
            if (!IsPostBack)
            {
                // check for IP Authentication
                bool isAuthenticated = new DebevoiseWebView.Code.Utility().checkAuthentication(Request.UserHostAddress);
                if (!isAuthenticated)
                {
                    Response.Redirect("Error/error.aspx");
                }
                collection = new System.Collections.Specialized.NameValueCollection();
                collection = Request.QueryString;
                if (collection != null && collection.Count > 0)
                {
                    Session["params"] = collection;
                }
            }           
        }

       protected void Unnamed1_Click(object sender, EventArgs e)
        {
            System.Collections.Specialized.NameValueCollection collection
                = new System.Collections.Specialized.NameValueCollection();
             System.Collections.Specialized.NameValueCollection collectionOld = (System.Collections.Specialized.NameValueCollection)Session["params"];          
            // getting textArea value
            if (textbox_query!=null && !string.IsNullOrEmpty(textbox_query.Value))
            {
                string[] paramsArray = textbox_query.Value.Split(new[] { '&' }).ToArray<string>();
                if (paramsArray != null && paramsArray.Count() > 0)
                {
                    for (int i = 0; i < paramsArray.Count(); i++)
                    {
                        string[] pair = paramsArray[i].Split(new[] { '=' }).ToArray<string>();
                        if (pair != null && pair.Count() >= 2)
                        {
                            if (collectionOld[pair[0].ToString()] == null)
                            {                                
                                 collection.Add(pair[0], pair[1]);
                            }
                        }
                    }
                }                      
            }
            //getting checkbox status
            if (insider.Checked)
            {
                collection.Add("InsiderFilings", "true");
            }
            else
            {
                collection.Add("InsiderFilings", "false");
            }
            Session["params2"] = collection;                   
        }
    }
        
       
}