﻿using Intelligize.ML.PDE;
using Intelligize.ML.SF;
using Intelligize.ML.SF.Commons;
using Intelligize.ML.SF.Dtos.Document;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;

namespace Intelligize.Webparts.App
{
    public partial class DocumentDownloader : System.Web.UI.Page
    {
        string _downloadFileName = "Document";

        string _format = string.Empty,
            _documentId = string.Empty,
            _documentType = string.Empty,
            _applicationType = string.Empty;

        List<DocumentDetails> _mergedDocument = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            _format = Request["Format"].ToString().ToLower();
            _documentId = Request["DocumentId"].ToString().ToLower();
            _documentType = Request["DocumentType"].ToString().ToLower();
            DocumentHandler docHandler = new DocumentHandler();
            // string documentIds, string format, string notes, string logoPath, string documentTypes, string userName, 
            //string billingReference, string applicationType, string documentsPageSizeLimit, string includeExhbits, 
            //string searchedText, bool isZipFile, bool isFullDocument, string primaryDocumentID, bool isIncludeCoverPage, 
            //string searchResultsLogID, bool isMultipleFiles

            //_mergedDocument = basicSearchSeavice.MergeDocuments(_documentId, _format, "", "", _documentType, "", "",
            //    _applicationType,
            //    "1", "true", "", false, false, "", true, "", false);

            Intelligize.ML.PDE.Entities.MappedInput printInput = new ML.PDE.Entities.MappedInput
            {
                DocumentIDs = _documentId,
                OutputFormat = _format,
                Notes = "",
                LogoPath = "",
                DocumentTypes = _documentType,
                UserName = "",
                BillingReference = "",
                ApplicationType = _applicationType,
                PageSize = "1",
                PrintExhibits = "1",                
                TextSearchWords = "",
                isZip = false,
                IsFullDocument = false,
                PrimaryDocumentID = "",
                IsIncludeCoverPage = true,
                SearchID = "",
                IsMultipleFilesAttachment = false,
                IncludeXBRL = false
            };

            _mergedDocument = docHandler.MergeDocuments(printInput);            
        }

        protected override void Render(HtmlTextWriter writer)
        {
            System.IO.StringWriter baseWriter = new System.IO.StringWriter();
            HtmlTextWriter htmlWriter = new HtmlTextWriter(baseWriter);
            base.Render(htmlWriter);

            if (_mergedDocument != null && _mergedDocument.Count > 0)
            {
                DocumentDetails document = _mergedDocument[0];
                if (_format.Equals("pdf"))
                {
                    _downloadFileName =
                        Utility.GetCustomFileName(document, false,
                            "pdf");
                    DownloadPDF(document.FileContents);
                }
                else if (_format.Equals("rtf"))
                {
                    _downloadFileName = Utility.GetCustomFileName(document, false,
                            "rtf");
                    DownloadRTF(document.FullContent);
                }
                else if (_format.Equals("HTM"))
                {
                    // DownloadHTM(document.f);
                }
            }
        }

        private void DownloadPDF(byte[] byteArrayPDF)
        {
            try
            {
                if (byteArrayPDF != null)
                {
                    string fileName = _downloadFileName;
                                        
                    Utility.ForceDownload(fileName,
                        this.Page.Response, this.Page.Request.UserAgent);
                    this.Page.Response.ContentType = "Application/pdf";
                    this.Page.Response.BinaryWrite(byteArrayPDF);
                    this.Page.Response.Charset = "";
                   // this.Page.Response.End();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                  //  this.Page.Response.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void DownloadRTF(string textRTF)
        {
            try
            {
                string fileName = _downloadFileName;
                
                Utility.ForceDownload(fileName, this.Page.Response,
                    this.Page.Request.UserAgent);
                this.Page.Response.ContentType = "Application/rtf";
                this.Page.Response.Write(textRTF);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
               // this.Page.Response.End();
               // this.Page.Response.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}