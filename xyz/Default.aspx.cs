﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Services;
using System.Web.UI.WebControls;
using Intelligize.Webparts.App.Code;
using System.Text.RegularExpressions;
using Intelligize.ML.Commons.Companies.Dtos;
using Intelligize.ML.IO.Commons;
using Intelligize.ML.IO.Commons.Search.Output;
using Intelligize.ML.IO.SF.Search.Output;
using Intelligize.ML.IO.Commons.Filters;
using Intelligize.Commons.IPValidator.Library;
using Intelligize.Commons.IPValidator.Library.DBHandlers;

namespace Intelligize.Webparts.App
{
    public partial class Default : System.Web.UI.Page
    {
        int _PageNo = 0;

        [WebMethod]
        public static string GetCompaniesAutoComplete(string filter)
        {
            try
            {
                List<ReferenceList> companies = new List<ReferenceList>();
                if (!filter.Equals("Not a valid filter"))
                {
                    if (filter == "apple inc")
                    {
                        var temp = 1;
                    }
                    CompanyCollection companyResult = Company.AutoCompleteCompanies(filter,
                        Applications.SF, CompanyAutocompleteCombo.BeginsWith, 25, 1, 25);
                    if (companyResult != null && companyResult.CompanyList != null &&
                        companyResult.CompanyList.Count > 0)
                    {
                        companies = companyResult.CompanyList.Select(obj => new ReferenceList
                        {
                            id = obj.CIK,
                            name = GetFullName(obj.DisplayName, obj.CIK, obj.Ticker),
                            companyName = obj.DisplayName,
                            ticker = obj.Ticker,
                        }).ToList();
                    }
                }
                System.Web.Script.Serialization.JavaScriptSerializer jsonSerializer =
                    new System.Web.Script.Serialization.JavaScriptSerializer();
                string jsonString = jsonSerializer.Serialize(companies);
                return jsonString;
            }
            catch (Exception ex)
            {
                File.AppendAllText(ConfigurationManager.AppSettings["Error_Log_file_path"] + "QC_APP_ERROR_LOG.txt",
                    System.Environment.NewLine + "Date=" + DateTime.Now.ToString() + System.Environment.NewLine +
                    " ERROR=" + ex.Message.ToString() + System.Environment.NewLine + "STACK_TRACE::" +
                    ex.StackTrace.ToString() + System.Environment.NewLine);
                return ex.Message.ToString() + ex.StackTrace;
            }
        }

        private static string GetFullName(string companyName, string cik, string ticker)
        {
            List<string> partialItems = new List<string>();
            partialItems.Add(companyName);
            partialItems.Add(ticker);
            partialItems.Add(cik);
            string html = "<div>" +
                          "<div style='float:right;padding-left: 5px;'><span style='padding-right:5px;'>" + ticker +
                          "</span style='padding-left:5px;'><span>" + cik + "</span></div>" +
                          "<div style='font-weight:bold'>" + companyName + "</div>" +
                          "</div>";
            string fullName = string.Join("&nbsp;&nbsp;&nbsp;&nbsp;",
                partialItems.Where(obj => !string.IsNullOrEmpty(obj)));
            return html;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string pageNo = Request.Params.Get("__EVENTARGUMENT"); // Getting this page no when pager is clicked
            if (!string.IsNullOrEmpty(pageNo))
            {
                Session["PageNo"] = pageNo;
            }

            string ipAddress = Request.ServerVariables["HTTP_X_CLIENTSIDE"];
            if (ipAddress != null)
            {
                Match matchedIP = Regex.Match(ipAddress, @"^[\d\.]+");
                if (matchedIP.Success == true)
                {
                    ipAddress = matchedIP.Value;
                }
                else
                {
                    ipAddress = Request.UserHostAddress;
                }
            }
            else
            {
                ipAddress = Request.UserHostAddress;
            }

            bool isIPValidationRequired =
                bool.Parse(ConfigurationManager.AppSettings["IsIPValidationRequired"].ToString());
            if (isIPValidationRequired == true)
            {
                bool isValidIp = IPHandler.CheckIP(ipAddress, 0);
                if (isValidIp == true)
                {
                    //Nothing to Do 
                }
                else
                {
                    // Redirect to errored Page
                    Response.Redirect("~/Error/UnautherizedAccess.html");
                    return;
                }
            }
            System.Collections.Specialized.NameValueCollection collection =
                new System.Collections.Specialized.NameValueCollection();
            System.Collections.Specialized.NameValueCollection collectionPost =
                new System.Collections.Specialized.NameValueCollection();
            string oldQueryString = string.Empty;
            int user = 0;
            if (Session != null && Session["SearchQuery"] != null)
            {
                oldQueryString = Session["SearchQuery"].ToString();
            }

            if (!IsPostBack)
            {
                bool isLocalHost = bool.Parse(ConfigurationManager.AppSettings["IsLocalHost"].ToString());
                if (isLocalHost == false)
                {                    
                    DBHelper.LogIPAddress(ipAddress, "Webparts");
                }
                populateList();
                //if (Request["email"] != null && Request["email"].ToString() != string.Empty && Request["password"] != null && Request["password"].ToString() != "")
                //{
                //    user = this.UserAuthentication(Request["email"].ToString(), Request["password"].ToString());
                //    if (user > 0)
                //    {
                //        Session["IsAuthenticated"] = "true";
                //        Session["encodedUser"] = Utility.EncodePassword(Request["user"].ToString());
                //    }
                //}
                collection = new System.Collections.Specialized.NameValueCollection();
                collection = Request.QueryString;
                // reflect to readonly property

                bool isSearchMode = false;
                if (collection.AllKeys.Contains("mode"))
                {
                    PropertyInfo isreadonly =
                        typeof (System.Collections.Specialized.NameValueCollection).GetProperty("IsReadOnly",
                            BindingFlags.Instance | BindingFlags.NonPublic);

                    // make collection editable
                    isreadonly.SetValue(collection, false, null);

                    if (collection["mode"].Equals("search", StringComparison.OrdinalIgnoreCase))
                    {
                        isSearchMode = true;
                        btnSearch.Text = "Search";
                    }
                    else
                    {
                        isSearchMode = false;
                        btnSearch.Text = "Filter";
                    }

                    collection.Remove("mode");
                    isreadonly.SetValue(collection, true, null);
                }
                else
                {
                    Session["NoMode"] = true;
                    isSearchMode = true; // It means no mode is given, and I have to consider default search
                    btnSearch.Text = "Search";
                }
                Session["isSearchMode"] = isSearchMode;
                if (collection != null && collection.Count > 0)
                {
                    if (Request["PageSize"] != null)
                    {
                        if (Convert.ToInt32(Request["PageSize"].ToString()) > 0 &&
                            Convert.ToInt32(Request["PageSize"].ToString()) <= 100)
                        {
                            Session["PageSize"] = Request["PageSize"];
                        }
                        else
                        {
                            Session["PageSize"] = "100";
                        }
                    }
                    else
                    {
                        Session["PageSize"] = "25";
                    }
                    Session["params"] = collection;
                }
                else
                {
                    Session["PageSize"] = "25";
                }
                Session["SearchQuery"] = Request.RawUrl;
                Session["AppName"] = "SF";
                Session["OldQueryString"] = oldQueryString;
            }
            else
            {
                if (userOptions != null && userOptions.SelectedIndex != 0)
                {
                    string SelectedValue = userOptions.Text.ToString();
                    collectionPost.Add("selectedFormValue", userOptions.SelectedItem.Value.ToString());
                    Session["params2"] = collectionPost;
                }
                if (!string.IsNullOrEmpty(txtCompany.Value))
                {
                    Session["companyRelInfo"] = txtCompany.Value;
                }
                if (Request.RawUrl.Contains("?"))
                {
                    collection = new System.Collections.Specialized.NameValueCollection();
                    collection = Request.QueryString;
                    if (collection != null && collection.Count > 0)
                    {
                        if (Request["PageSize"] != null)
                        {
                            if (Convert.ToInt32(Request["PageSize"].ToString()) > 0 &&
                                Convert.ToInt32(Request["PageSize"].ToString()) <= 100)
                            {
                                Session["PageSize"] = Request["PageSize"];
                            }
                            else
                            {
                                Session["PageSize"] = "100";
                            }
                        }
                        else
                        {
                            Session["PageSize"] = "25";
                        }
                        Session["params"] = collection;
                    }
                    else
                    {
                        Session["PageSize"] = "25";
                    }
                    Session["SearchQuery"] = Request.RawUrl;
                    Session["AppName"] = "SF";
                }
                Session["OldQueryString"] = oldQueryString;
            }
        }

        public List<string> GetColumnsSequence()
        {
            List<string> columns = new List<string>();
            //if (!string.IsNullOrEmpty(Request["ReturnField0"]))
            //{
            //    columns.Add(Request["ReturnField0"].Replace(" ", string.Empty));
            //}
            //else if (!columns.Contains("Filing Date"))
            //{
            columns.Add("Filing Date");
            //}

            //if (!string.IsNullOrEmpty(Request["ReturnField1"]))
            //{
            //    columns.Add(Request["ReturnField1"].Replace(" ", string.Empty));
            //}
            //else if(!columns.Contains("Form"))
            //{
            columns.Add("Form");
            // }


            //if (!string.IsNullOrEmpty(Request["ReturnField2"]))
            //{
            //    columns.Add(Request["ReturnField2"].Replace(" ",string.Empty));
            //}
            //else if (!columns.Contains("Company Name"))
            //{
            columns.Add("Company Name");
            // }          
            columns.Add("Download");

            return columns;
        }

        public List<Document> GetResults(string clickedPageNo)
        {
            bool isSearchMode = false;
            if (Session["isSearchMode"] != null)
            {
                isSearchMode = (bool) Session["isSearchMode"];
            }
            string companyRelInfo = string.Empty;
            if (Session["companyRelInfo"] != null)
            {
                companyRelInfo = (string) Session["companyRelInfo"];
            }

            NameValueCollection queryParams = null;
            if (Session["params"] != null)
            {
                queryParams = (NameValueCollection) Session["params"];
            }
            NameValueCollection formTypeFilter = null;
            if (Session["params2"] != null)
            {
                formTypeFilter = (NameValueCollection) Session["params2"];
            }
            List<string> columns = GetColumnsSequence();
            List<Document> results = new List<Document>();

            ArrayList op = FiltersMapper.MapParam(queryParams, formTypeFilter,
                Server.UrlDecode(Session["OldQueryString"].ToString()),
                Server.UrlDecode(Session["SearchQuery"].ToString()), isSearchMode, companyRelInfo, clickedPageNo);
            results = GetMappedResults(op);

            return results;
        }

        public List<Document> GetMappedResults(ArrayList obj)
        {
            List<Document> customObject = new List<Document>();


            Output<Output> output = null;
            if (Session["AppName"] != null &&
                Session["AppName"].ToString().Equals("SF", StringComparison.OrdinalIgnoreCase))
            {
                output = (Output<Output>)obj[0];
            }


            List<string> columns = GetColumnsSequence();
            List<string> results = new List<string>();
            string docType = "primary";
            string intelUrl = ConfigurationManager.AppSettings["URL_Apps"].ToString();
            if (output != null && output.SearchOutput != null && output.SearchOutput.Count() > 0)
            {
                foreach (Output objectOuput in output.SearchOutput)
                {
                    Document singleObject = new Document();
                    singleObject.TotalRecords = output.TotalMatchedCount;
                    singleObject.PageNo = output.PageNumber;
                    for (int i = 0; i < columns.Count(); i++)
                    {
                        if (columns[i].Equals("Filing Date", StringComparison.OrdinalIgnoreCase))
                        {
                            singleObject.FilingDate = objectOuput.FilingDate;
                        }
                        else if (columns[i].Equals("Form", StringComparison.OrdinalIgnoreCase))
                        {
                            singleObject.FormType = objectOuput.FormType;
                            //if (!string.IsNullOrEmpty(objectOuput.ExhibitNumber) && !string.IsNullOrEmpty(objectOuput.ExhibitName))
                            //{
                            //    singleObject.Exhibit = objectOuput.ExhibitNumber + " -" + objectOuput.ExhibitName;
                            //}
                        }
                        else if (columns[i].Equals("Company Name", StringComparison.OrdinalIgnoreCase))
                        {
                            singleObject.CompanyName = objectOuput.CompanyName;
                        }
                        else if (columns[i].Equals("Download", StringComparison.OrdinalIgnoreCase))
                        {
                            singleObject.DocumentId = objectOuput.DocumentId.ToString();
                        }
                    }

                    if (objectOuput.IsExhibit)
                    {
                        //if (Session["IsAuthenticated"] == null || !Session["IsAuthenticated"].ToString().Equals("true"))
                        //{
                        singleObject.DocumentUrl = intelUrl + "?Document=&DocumentType=primary&Action=exhibit&OtherId=" +
                                                   singleObject.DocumentId + "&FormType=" + objectOuput.FormType +
                                                   "&Keyword=&PageHeading=&Application=SF&link=3.0";
                        //}
                        //else
                        //{
                        //    singleObject.DocumentUrl = intelUrl + "?iswpLink=true&wpid=" + Session["encodedUser"].ToString() + "&Document=&DocumentType=primary&Action=exhibit&OtherId=" + singleObject.DocumentId + "&FormType=" + objectOuput.FormType + "&Application=SC&link=3.0";
                        //}
                        singleObject.PDFDocumentUrl = "DocumentDownloader.aspx?DocumentID=" + objectOuput.DocumentId +
                                                      "&DocumentType=exhibit&format=pdf";
                        singleObject.RTFDocumentUrl = "DocumentDownloader.aspx?DocumentID=" + objectOuput.DocumentId +
                                                      "&DocumentType=exhibit&format=rtf";
                    }
                    else
                    {
                        singleObject.PDFDocumentUrl = "DocumentDownloader.aspx?DocumentID=" + objectOuput.DocumentId +
                                                      "&DocumentType=primary&format=pdf";
                        singleObject.RTFDocumentUrl = "DocumentDownloader.aspx?DocumentID=" + objectOuput.DocumentId +
                                                      "&DocumentType=primary&format=rtf";

                        //if (Session["IsAuthenticated"] == null || !Session["IsAuthenticated"].ToString().Equals("true"))
                        //{
                        singleObject.DocumentUrl = intelUrl + "?Document=" + objectOuput.DocumentId +
                                                   "&DocumentType=primary&Action=&OtherId=&FormType=" +
                                                   objectOuput.FormType +
                                                   "&Keyword=&PageHeading=&Application=SF&link=3.0";
                        //}
                        //else
                        //{
                        //    singleObject.DocumentUrl = intelUrl + "?iswpLink=true&wpid=" + Session["encodedUser"].ToString() + "&Document=" + singleObject.DocumentId + "&DocumentType=primary&Action=&OtherId=&FormType=" + objectOuput.FormType + "&Application=SC&link=3.0";
                        //}
                    }


                    customObject.Add(singleObject);
                }
            }
            return customObject;
        }

        public List<FormTypes> GetComboBoxValues()
        {
            string myConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_db_sf"].ToString();
            List<FormTypes> list = new List<FormTypes>();

            if ((Request["filter_formtype"] != null || Request["Filter_FormType"] != null)
                && (Request["filter_formgroup"] != null || Request["Filter_FormGroup"] != null))
            {
                #region[Both FormType and groups]

                string form = string.Empty;
                if (!string.IsNullOrEmpty(Request["filter_formtype"]))
                {
                    form = Request["filter_formtype"].ToString();
                }
                else
                {
                    form = Request["Filter_FormType"].ToString();
                }
                string[] ids = form.Split(new[] {';'}).ToArray<string>();
                foreach (string id in ids)
                {
                    FormTypes group = new FormTypes();
                    group.Value = id;
                    list.Add(group);
                }


                if (!string.IsNullOrEmpty(Request["filter_formgroup"]))
                {
                    form = Request["filter_formgroup"].ToString();
                }
                else
                {
                    form = Request["Filter_FormGroup"].ToString();
                }
                ids = form.Split(new[] {';'}).ToArray<string>();
                foreach (string id in ids)
                {
                    FormTypes group = new FormTypes();
                    group.Key = id;
                    group.Value = id;
                    ////group.Value = new DebevoiseWebView.Code.Utility().GetSECGroupName(id);
                    list.Add(group);
                }

                if (list != null)
                {
                    list.AddRange(GetAllForms(form));
                }
                else
                {
                    list = GetAllForms(form);
                }

                #endregion
            }

            if (Request["filter_formtype"] == null && Request["Filter_FormType"] == null
                && Request["filter_formgroup"] == null && Request["Filter_FormGroup"] == null)
            {
                #region[ No FormType No FormGroup]

                string query = "select group_name from tbl_ref_form_group where show_in_gui=1";
                System.Data.DataSet dtList = Intelligize.Commons.CommonHandler.DBHandler.ReadTable(query,
                    myConnectionString);
                if (dtList != null && dtList.Tables != null && dtList.Tables.Count > 0)
                {
                    for (int i = 0; i < dtList.Tables[0].Rows.Count; i++)
                    {
                        FormTypes group = new FormTypes();
                        group.Value = dtList.Tables[0].Rows[i][0].ToString();
                        //// group.Value =GetOtherFormGroup(dtList.Tables[0].Rows[i][0].ToString());
                        list.Add(group);
                    }
                }
                query = "select TreeType from tbl_ref_form_type where showingui=1";
                dtList = Intelligize.Commons.CommonHandler.DBHandler.ReadTable(query, myConnectionString);
                if (dtList != null && dtList.Tables != null && dtList.Tables.Count > 0)
                {
                    for (int i = 0; i < dtList.Tables[0].Rows.Count; i++)
                    {
                        FormTypes group = new FormTypes();
                        group.Value = dtList.Tables[0].Rows[i][0].ToString();
                        list.Add(group);
                    }
                }

                #endregion
            }

            else if ((Request["filter_formtype"] != null || Request["Filter_FormType"] != null)
                     && Request["filter_formgroup"] == null && Request["Filter_FormGroup"] == null)
            {
                #region[Only formTypes No FormGroup]

                if ((Request["filter_formtype"] != null) || (Request["Filter_FormType"] != null))
                {
                    string form = string.Empty;
                    if (!string.IsNullOrEmpty(Request["filter_formtype"]))
                    {
                        form = Request["filter_formtype"].ToString();
                    }
                    else
                    {
                        form = Request["Filter_FormType"].ToString();
                    }
                    string[] ids = form.Split(new[] {';'}).ToArray<string>();
                    foreach (string id in ids)
                    {
                        FormTypes group = new FormTypes();
                        group.Value = id;
                        list.Add(group);
                    }
                }
                else
                {
                    //list = new List<ComboBoxObject>().Add(new ComboBoxObject{ Value = })
                }

                #endregion
            }

            else if (Request["filter_formtype"] == null && Request["Filter_FormType"] == null
                     && (Request["filter_formgroup"] != null || Request["Filter_FormGroup"] != null))
            {
                #region[Only FormGroups No FormType]

                string form = string.Empty;
                if (!string.IsNullOrEmpty(Request["filter_formgroup"]))
                {
                    form = Request["filter_formgroup"].ToString();
                }
                else
                {
                    form = Request["Filter_FormGroup"].ToString();
                }
                string[] ids = form.Split(new[] {';'}).ToArray<string>();
                foreach (string id in ids)
                {
                    FormTypes group = new FormTypes();
                    //// group.Value = new DebevoiseWebView.Code.Utility().GetSECGroupName(id);
                    group.Value = id;
                    list.Add(group);
                }

                if (list != null)
                {
                    list.AddRange(GetAllForms(form));
                }
                else
                {
                    list = GetAllForms(form);
                }

                #endregion
            }

            return list;
        }

        public List<FormTypes> GetAllForms(string groupNames)
        {
            string myConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString_db_sf"].ToString();
            FiltersMapper util = new FiltersMapper();
            List<FormTypes> formList = new List<FormTypes>();
            List<Entity> entityList = util.GetFormGroups(groupNames);
            if (entityList != null && entityList.Count > 0)
            {
                string groupIds = string.Join(",", entityList.Select(ob => ob.Key).ToArray());
                string query = @"select treetype from tbl_link_form_group_to_form_type
                join tbl_ref_form_type rt on rt.pk_id = fk_master_tree_type_id 
                where fk_form_groups_name_id in (" + groupIds + ") and rt.ShowInGUI=1";

                System.Data.DataSet dtList = Intelligize.Commons.CommonHandler.DBHandler.ReadTable(query,
                    myConnectionString);
                if (dtList != null && dtList.Tables != null && dtList.Tables.Count > 0)
                {
                    for (int i = 0; i < dtList.Tables[0].Rows.Count; i++)
                    {
                        FormTypes group = new FormTypes();
                        group.Value = dtList.Tables[0].Rows[i][0].ToString();
                        formList.Add(group);
                    }
                }
            }
            return formList;
        }

        public void populateList()
        {
            List<FormTypes> list = this.GetComboBoxValues();
            if (list != null && list.Count > 0)
            {
                ListItem item = new ListItem();
                item.Text = "All Forms";
                item.Value = "0";
                item.Selected = true;
                userOptions.Items.Add(item);
                for (int i = 0; i < list.Count; i++)
                {
                    item = new ListItem();
                    if (!string.IsNullOrEmpty(list[i].Key))
                    {
                        item.Text = list[i].Key;
                    }
                    else
                    {
                        item.Text = list[i].Value;
                    }
                    item.Value = list[i].Value;
                    userOptions.Items.Add(item);
                }
                Session["IsForm"] = "true";
            }
            else
            {
                Session["IsForm"] = "false";
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
        }
    }
}